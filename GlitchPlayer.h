//
//  GlitchPlayer.h
//  GlitchPlayer
//
//  Created by 'Ili Butterfield on 1/2/10.
//  Copyright (c) 2016 All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>


@interface GlitchPlayer : NSObject {
    AUGraph audioGraph;
    AUNode mixerNode;
    NSMutableArray *channels;
    float glitchLevel;
}

- (void)setUpAudioGraph;

- (void)loadFiles:(NSArray *)paths;

- (void)startPlaying;
- (void)stopPlaying;

- (void)setGlitchLevel:(float)gl;

@end
