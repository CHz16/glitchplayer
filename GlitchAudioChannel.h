//
//  GlitchAudioChannel.h
//  GlitchPlayer
//
//  Created by 'Ili Butterfield on 1/6/10.
//  Copyright (c) 2016. All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>
#import <AudioUnit/AudioUnit.h>


@interface GlitchAudioChannel : NSObject {
    AudioFileID audioFile;
    AUNode filePlayerNode;
    AudioUnit filePlayerUnit;
    AUNode effectNode;

    AUGraph audioGraph;
    AUNode mixerNode;
    UInt32 bus;

    int sampleRate;

    float glitchLevel;
    BOOL active;
    NSTimer *swapTimer;
}

- (id)initWithPath:(NSString *)path inAudioGraph:(AUGraph)ag intoMixerNode:(AUNode)mn bus:(UInt32)b;

- (void)schedule;
- (void)remove;
- (void)setGlitchLevel:(float)glitchLevel;

- (void)swapEffect:(NSTimer *)theTimer;
- (void)scheduleSwap;

- (void)createEffect;
- (void)createPassthroughEffect;
- (void)createGlitchEffect;
- (void)connectEffect;
- (void)disconnectEffect;

@end
