//
//  GlitchPlayer.m
//  GlitchPlayer
//
//  Created by 'Ili Butterfield on 1/2/10.
//  Copyright (c) 2016. All rights reserved.
//

#import "GlitchPlayer.h"

#import <AudioUnit/AudioUnit.h>
#import <CoreServices/CoreServices.h>
#import "GlitchAudioChannel.h"


@implementation GlitchPlayer

- (id)init {
    if ((self = [super init])) {
        [self setUpAudioGraph];
        channels = [[NSMutableArray alloc] init];
        glitchLevel = 0.0f;
    }
    return self;
}

- (void)dealloc {
    [channels release]; channels = nil;
    DisposeAUGraph(audioGraph);

    [super dealloc];
}


// Creates the audio graph and adds the default output and mixer nodes.
- (void)setUpAudioGraph {
    NewAUGraph(&audioGraph);

    // Create the output node.
    AudioComponentDescription cd;
    AUNode outputNode;
    cd.componentType = kAudioUnitType_Output;
    cd.componentSubType = kAudioUnitSubType_DefaultOutput;
    cd.componentManufacturer = kAudioUnitManufacturer_Apple;
    cd.componentFlags = 0;
    cd.componentFlagsMask = 0;
    AUGraphAddNode(audioGraph, &cd, &outputNode);

    // Create the mixer node and connect it to the output node.
    cd.componentType = kAudioUnitType_Mixer;
    cd.componentSubType = kAudioUnitSubType_StereoMixer;
    cd.componentManufacturer = kAudioUnitManufacturer_Apple;
    cd.componentFlags = 0;
    cd.componentFlagsMask = 0;
    AUGraphAddNode(audioGraph, &cd, &mixerNode);
    AUGraphConnectNodeInput(audioGraph, mixerNode, 0, outputNode, 0);

    AUGraphOpen(audioGraph);
}

- (void)loadFiles:(NSArray *)paths {
    [channels removeAllObjects];

    for (UInt32 i = 0; i < [paths count]; i++) {
        GlitchAudioChannel *channel = [[GlitchAudioChannel alloc] initWithPath:[paths objectAtIndex:i]
                                                                  inAudioGraph:audioGraph
                                                                 intoMixerNode:mixerNode
                                                                           bus:i];
        [channels addObject:channel];
        [channel release];
    }

    [self setGlitchLevel:0.0f];
}

- (void)startPlaying {
    AUGraphInitialize(audioGraph);

    for (GlitchAudioChannel *channel in channels) {
        [channel schedule];
    }

    AUGraphStart(audioGraph);
    //CAShow(audioGraph);
}

- (void)stopPlaying {
    AUGraphStop(audioGraph);
    AUGraphUninitialize(audioGraph);

    for (GlitchAudioChannel *channel in channels) {
        [channel remove];
    }
}

- (void)setGlitchLevel:(float)gl {
    NSLog(@"glitch level: %f", gl);
    glitchLevel = gl;
    for (GlitchAudioChannel *channel in channels) {
        [channel setGlitchLevel:glitchLevel];
    }
}

@end
