//
//  main.h
//  GlitchPlayer
//
//  Created by 'Ili Butterfield on 1/2/10.
//  Copyright (c) 2016. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <signal.h>
#import "GlitchPlayer.h"

@class GlitchPlayerTwiddler;

NSMutableArray *paths;
GlitchPlayer *gp;
GlitchPlayerTwiddler *gpt;

@interface GlitchPlayerTwiddler : NSObject {
    float glitchLevel;
}

- (void)increaseGlitchLevel:(NSTimer *)theTimer;
- (void)reset;

@end

@implementation GlitchPlayerTwiddler

- (id)init {
    if ((self = [super init])) {
        [self reset];
    }
    return self;
}

- (void)increaseGlitchLevel:(NSTimer *)theTimer {
    glitchLevel = glitchLevel + 0.01f;
    if (glitchLevel > 1.0f) {
        glitchLevel = 1.0f;
    }
    [gp setGlitchLevel:glitchLevel];
}

- (void)reset {
    glitchLevel = 0.0f;
}

@end


void restart(int parameter);
void restart(int parameter) {
    [gp stopPlaying];
    [gpt reset];
    [gp loadFiles:paths];
    [gp startPlaying];
}

int main (int argc, char *argv[]) {
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

    paths = [NSMutableArray arrayWithCapacity:(unsigned int)(argc - 1)];
    for (int i = 1; i < argc; i++) {
        [paths addObject:[NSString stringWithCString:argv[i] encoding:[NSString defaultCStringEncoding]]];
    }

    gp = [[GlitchPlayer alloc] init];
    [gp loadFiles:paths];
    [gp startPlaying];

    gpt = [[GlitchPlayerTwiddler alloc] init];

    signal(SIGTSTP, restart);

    NSRunLoop *rl = [NSRunLoop currentRunLoop];
    [NSTimer scheduledTimerWithTimeInterval:5.0
                                     target:gpt
                                   selector:@selector(increaseGlitchLevel:)
                                   userInfo:nil
                                    repeats:YES];
    [rl run];

    [gpt release];
    [gp release];
    [pool drain];
    return 0;
}
