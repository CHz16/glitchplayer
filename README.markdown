GlitchPlayer
============

A half-baked music engine with adjustable glitchiness that I wrote six years ago for a game I may or may not ever actually make. That was just a proof-of-concept test to see how well the glitch idea worked, so the code is kind of janky and there's a bunch of debug logging in it and it doesn't support some real basic features like pausing and arbitrary loop points. But it does produce an auditory descent into Hell, so that's cool!

Here are a few sample tunes with the glitch level slowly increasing from 0 to 1 over the course of about two and a half minutes, with an extra 30 seconds or so of max glitch at the end for your listening "pleasure":

* [*Mega Man 5* - "DARKMAN STAGE"](http://quiteajolt.com/projects/glitchplayer/Mega%20Man%205%20-%20DARKMAN%20STAGE.mp3)
* [*Tetris Attack* - "Forest Stage"](http://quiteajolt.com/projects/glitchplayer/Tetris%20Attack%20-%20Forest%20Stage.mp3)
* [*Thunder Force IV* - "The Sky Line"](http://quiteajolt.com/projects/glitchplayer/Thunder%20Force%20IV%20-%20The%20Sky%20Line.mp3)


How This Thing Does the Thing It Does
-------------------------------------

All this does is apply random audio effects to each of the channels of the music. As the glitch level goes up, so does the probability of a channel getting an effect and the severity of that effect. Pretty simple, but it works well!

"How does it apply different effects to different channels of the music?" Well, you have to supply it multiple files, each containing a different channel, and it'll play them all back at the same time. :V


The Binary This Builds
----------------------

The target this project builds is a command line thingummy that loads up every argument you pass it as a channel for the music player, and it slowly increases the glitch level over time. There aren't any arguments to change the glitch parameters, so you'll have to edit `main.m` if you want it to progress differently than 0.01 every 5 seconds (the increment is in `[GlitchPlayerTwiddler increaseGlitchLevel:]` and the duration is in the `[NSTimer scheduledTimerWithTimeInterval:::::]` call at the end).

You can also send `SIGTSTP` (`control-Z`) to reset the song to the beginning and the glitch level to 0. I'm not entirely sure why I thought that would be useful, but it's there!

Make sure the audio files for each channel are exactly the same length, because otherwise they'll desync when looping.


Using the Code
--------------

You probably don't want to use this code, because as I mentioned earlier it was just a quick test and barely works. But if you hate yourself, it's pretty easy: allocate one, jam an array of file paths as strings into it, tell it to `startPlaying`, and then just call `setGlitchLevel:` on it whenever seems appropriate.

To kill the music, tell it to `stopPlaying`. From there, you can throw it away or tell it to load up a new bunch of files and `startPlaying` again. You **do need** to make it load files again, even if it's just the same set of files as from the last time, because `stopPlaying` is pretty destructive. I said this was janky, remember? Don't actually use this.


Licensing
---------

This code is released under MIT (see LICENSE.txt).
