/*
 *  randomVariables.h
 *  GlitchPlayer
 *
 *  Created by 'Ili Butterfield on 1/7/10.
 *  Copyright (c) 2016. All rights reserved.
 *
 */

#ifndef RANDOMVARIABLES_H
#define RANDOMVARIABLES_H

// Returns an random integer from a linear distribution (more or less) in the range [min, max].
// All values are equally likely.
int linearRandomInteger(int min, int max);

// Returns an random decimal from a linear distribution (more or less) in the range [min, max].
// All values are equally likely.
float linearRandomDecimal(float min, float max);


// Returns an integer from a quadratic distribution (more or less), in the range [min, max].
// Concave up, centered at the midpoint, so values closer to the ends are more likely.
// |        |
//  \      /
//   `-__-'
float quadraticRandomDecimalUp(float min, float max);

// Returns an integer from a quadratic distribution (more or less), in the range [min, max].
// Concave down, centered at the midpoint, so values closer to the middle are more likely.
//   .---.
//  /     \
// |       |
float quadraticRandomDecimalDown(float min, float max);

// Returns an integer from a quadratic distribution (more or less), in the range [min, max].
// Concave down, centered at the left, so values closer to min are more likely.
// ---__
//       \
//        |
float quadraticRandomDecimalTailIn(float min, float max);

// Returns an integer from a quadratic distribution (more or less), in the range [min, max].
// Concave down, centered at the right, so values closer to max are more likely.
//    __---
//  /
// |
float quadraticRandomDecimalTailOut(float min, float max);

#endif
