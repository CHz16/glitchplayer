/*
 *  randomVariables.c
 *  GlitchPlayer
 *
 *  Created by 'Ili Butterfield on 1/7/10.
 *  Copyright (c) 2016. All rights reserved.
 *
 */

#include "randomVariables.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int linearRandomInteger(int min, int max) {
    unsigned int range = (unsigned int)(max - min);
    if (range == 0) {
        return min;
    }

    return min + (int)arc4random_uniform(range + 1);
}

float linearRandomDecimal(float min, float max) {
    float range = max - min;
    if (range == 0) {
        return min;
    }

    // 0xFFFFFFFF = 2^32 - 1 = max output of arc4random()
    return min + ((float)arc4random() / 0xFFFFFFFF) * range;
}


// Each of the quadratic random functions below generates a random number according to a
// parabolic probability density function of the form:
//   f(x) = a * (x - b)^2 + c if 0 ≤ x ≤ 1, 0 otherwise
//
// To generate the random numbers, we do some inverse transform sampling, as explained here:
// <http://www.av8n.com/physics/arbitrary-probability.htm>
//   1). Calculate the cumulative distribution function F(x) by integrating and then solving for
//       C by setting F(0) = 0. This ends up being F(x) = a/3 * (x - b)^3 + cx + ab^3/3
//   2). Calculate g(x) = F⁻¹(x). F(x) will always be invertible over the range [0, 1], since as
//       a cumulative distribution function it's monotonically increasing, but in the general
//       case its inverse won't have a nice, closed form. So, we'll approximate it within some
//       error threshold with a simple binary search.
//   3). We can then generate random numbers following the distribution f by calculating g(x) for
//       random numbers uniformly generated between 0 and 1.

float randomNumberFromParabolicDistribution(float a, float b, float c);
float randomNumberFromParabolicDistribution(float a, float b, float c) {
    static float errorThreshold = .000001f;

    // 0xFFFFFFFF = 2^32 - 1 = max output of arc4random()
    float y = (float)arc4random() / 0xFFFFFFFF;

    float rangeMin = 0, rangeMax = 1;
    float x;
    while (1) {
        x = (rangeMax + rangeMin) / 2;
        float guessY = (a / 3) * (x - b) * (x - b) * (x - b) + c * x + a * b * b * b / 3;

        if (fabsf(y - guessY) < errorThreshold) {
            break;
        } else if (guessY > y) {
            rangeMax = x;
        } else {
            rangeMin = x;
        }
    }

    return x;
}

float quadraticRandomDecimalUp(float min, float max) {
    float range = max - min;
    if (range == 0) {
        return min;
    }

    // f(x) = 9(x - .5)^2 + .25
    // Concave up parabola such that the center f(.5) = .25 and the area under [0, 1] = 1
    return min + randomNumberFromParabolicDistribution(9, .5, .25) * range;
}

float quadraticRandomDecimalDown(float min, float max) {
    float range = max - min;
    if (range == 0) {
        return min;
    }

    // f(x) = -6(x - .5)^2 + 1.5
    // Concave down parabola such that f(0) = f(1) = 0 and the area under [0, 1] = 1
    return min + randomNumberFromParabolicDistribution(-6, .5, 1.5) * range;
}

float quadraticRandomDecimalTailIn(float min, float max) {
    float range = max - min;
    if (range == 0) {
        return min;
    }

    // f(x) = -1.5x^2 + 1.5
    // Concave down parabola such that the center is at 0, f(1) = 0, and the area under [0, 1] = 1
    return min + randomNumberFromParabolicDistribution(-1.5, 0, 1.5) * range;
}

float quadraticRandomDecimalTailOut(float min, float max) {
    float range = max - min;
    if (range == 0) {
        return min;
    }

    // f(x) = -1.5(x - 1)^2 + 1.5
    // Concave down parabola such that the center is at 1, f(0) = 0, and the area under [0, 1] = 1
    return min + randomNumberFromParabolicDistribution(-1.5, 1, 1.5) * range;
}
