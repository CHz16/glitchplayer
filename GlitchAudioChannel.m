//
//  GlitchAudioChannel.m
//  GlitchPlayer
//
//  Created by 'Ili Butterfield on 1/6/10.
//  Copyright (c) 2016. All rights reserved.
//

#import "GlitchAudioChannel.h"

#import <CoreServices/CoreServices.h>

#import "randomVariables.h"


static const float EFFECT_DURATION_MEDIAN = 1.0f;
static const float EFFECT_DURATION_MAX_VARIATION = 0.5f;
static const float DELAY_MIN_INTERVAL = 0.0f;
static const float DELAY_MAX_INTERVAL = 0.05f;
static const int HIGHPASS_MIN_FREQ = 100;
static const int HIGHPASS_MAX_FREQ = 3000;
static const float PLAYBACK_RATE_MAX_VARIATION = 0.2f;
static const float SAMPLE_DELAY_MIN_OFFSET = 0.01f;
static const float SAMPLE_DELAY_MAX_OFFSET = 0.15f;
static const float DYNAMICS_PROCESSOR_MIN_VARIATION = 3.0f;
static const float DYNAMICS_PROCESSOR_MAX_VARIATION = 10.0f;
static const float PANNING_MAX_VARIATION = 0.5f;

@implementation GlitchAudioChannel

- (id)initWithPath:(NSString *)path inAudioGraph:(AUGraph)ag intoMixerNode:(AUNode)mn bus:(UInt32)b {
    if ((self = [super init])) {
        audioGraph = ag;
        mixerNode = mn;
        bus = b;

        glitchLevel = 0.0f;
        active = NO;

        // Create the file player node.
        AudioComponentDescription cd;
        cd.componentType = kAudioUnitType_Generator;
        cd.componentSubType = kAudioUnitSubType_AudioFilePlayer;
        cd.componentManufacturer = kAudioUnitManufacturer_Apple;
        cd.componentFlags = 0;
        cd.componentFlagsMask = 0;
        AUGraphAddNode(audioGraph, &cd, &filePlayerNode);
        AUGraphNodeInfo(audioGraph, filePlayerNode, NULL, &filePlayerUnit);

        // Load the audio file and send it to the file player unit.
        NSURL *fileURL = [NSURL fileURLWithPath:path];
        AudioFileOpenURL((CFURLRef)fileURL, fsRdPerm, 0, &audioFile);
        AudioUnitSetProperty(filePlayerUnit, kAudioUnitProperty_ScheduledFileIDs, kAudioUnitScope_Global, 0, &audioFile, sizeof(audioFile));

        // Find the sample rate of the audio data. We use this in the desync effect calculation.
        AudioStreamBasicDescription bsd;
        UInt32 descriptionSize;
        AudioFileGetPropertyInfo(audioFile, kAudioFilePropertyDataFormat, &descriptionSize, NULL);
        AudioFileGetProperty(audioFile, kAudioFilePropertyDataFormat, &descriptionSize, &bsd);
        sampleRate = (int)bsd.mSampleRate;
    }
    return self;
}

- (void)dealloc {
    [self remove];

    [super dealloc];
}


- (void)schedule {
    [self createEffect];
    active = YES;

    // Set up the play region for the audio file.
    ScheduledAudioFileRegion playRegion;
    playRegion.mTimeStamp.mFlags = kAudioTimeStampSampleTimeValid;
    playRegion.mTimeStamp.mSampleTime = 0;
    playRegion.mCompletionProc = NULL;
    playRegion.mCompletionProcUserData = NULL;
    playRegion.mAudioFile = audioFile;
    playRegion.mLoopCount = (UInt32)-1; // loop "forever"
    playRegion.mStartFrame = 0;
    playRegion.mFramesToPlay = (UInt32)-1; // play every frame
    AudioUnitSetProperty(filePlayerUnit, kAudioUnitProperty_ScheduledFileRegion, kAudioUnitScope_Global, 0, &playRegion, sizeof(playRegion));

    // Prime the file player.
    UInt32 primeFrames = 0;
    AudioUnitSetProperty(filePlayerUnit, kAudioUnitProperty_ScheduledFilePrime, kAudioUnitScope_Global, 0, &primeFrames, sizeof(primeFrames));

    // Set the start time for the file player.
    AudioTimeStamp startTime;
    startTime.mFlags = kAudioTimeStampSampleTimeValid;
    startTime.mSampleTime = -1; // next render cycle
    AudioUnitSetProperty(filePlayerUnit, kAudioUnitProperty_ScheduleStartTimeStamp, kAudioUnitScope_Global, 0, &startTime, sizeof(startTime));

    [self scheduleSwap];
}

- (void)remove {
    if (active) {
        [self disconnectEffect];
        active = NO;
        [swapTimer invalidate];
    }
    if (filePlayerNode) {
        AUGraphRemoveNode(audioGraph, filePlayerNode);
        AudioFileClose(audioFile);
        filePlayerNode = 0;
    }
}

- (void)setGlitchLevel:(float)gl {
    glitchLevel = gl;
}


- (void)swapEffect:(NSTimer *)theTimer {
    if (!active) {
        return;
    }

    [self disconnectEffect];
    [self createEffect];

    [self scheduleSwap];
}

- (void)scheduleSwap {
    float range = EFFECT_DURATION_MAX_VARIATION * glitchLevel;
    NSTimeInterval swapDelay = quadraticRandomDecimalDown(EFFECT_DURATION_MEDIAN - range, EFFECT_DURATION_MEDIAN + range);

    swapTimer = [NSTimer scheduledTimerWithTimeInterval:swapDelay
                                                 target:self
                                               selector:@selector(swapEffect:)
                                               userInfo:nil
                                                repeats:NO];
}


- (void)createEffect {
    if (linearRandomDecimal(0, 1) < glitchLevel) {
        [self createGlitchEffect];
    } else {
        [self createPassthroughEffect];
    }    
    AUGraphUpdate(audioGraph, NULL);
}

- (void)createPassthroughEffect {
    NSLog(@"bus %d: passthrough", bus);

    AudioComponentDescription cd;
    cd.componentType = kAudioUnitType_Effect;
    cd.componentSubType = kAudioUnitSubType_SampleDelay;
    cd.componentManufacturer = kAudioUnitManufacturer_Apple;
    cd.componentFlags = 0;
    cd.componentFlagsMask = 0;
    AUGraphAddNode(audioGraph, &cd, &effectNode);

    [self connectEffect];
}

- (void)createGlitchEffect {
    int effect = linearRandomInteger(0, 5);

    if (effect == 0) {
        // Delay effect.
        float delay = quadraticRandomDecimalTailIn(DELAY_MIN_INTERVAL, DELAY_MIN_INTERVAL + (DELAY_MAX_INTERVAL - DELAY_MIN_INTERVAL) * glitchLevel);
        NSLog(@"bus %d: delay of %f", bus, delay);

        AudioUnit effectUnit;
        AudioComponentDescription cd;
        cd.componentType = kAudioUnitType_Effect;
        cd.componentSubType = kAudioUnitSubType_Delay;
        cd.componentManufacturer = kAudioUnitManufacturer_Apple;
        cd.componentFlags = 0;
        cd.componentFlagsMask = 0;
        AUGraphAddNode(audioGraph, &cd, &effectNode);
        AUGraphNodeInfo(audioGraph, effectNode, NULL, &effectUnit);
        AudioUnitSetParameter(effectUnit, kDelayParam_DelayTime, kAudioUnitScope_Global, 0, delay, 0);
    } else if (effect == 1) {
        // Highpass filter.
        float cutoff = linearRandomDecimal(HIGHPASS_MIN_FREQ, HIGHPASS_MIN_FREQ + (HIGHPASS_MAX_FREQ - HIGHPASS_MIN_FREQ) * glitchLevel);
        NSLog(@"bus %d: highpass of %fHz", bus, cutoff);

        AudioUnit effectUnit;
        AudioComponentDescription cd;
        cd.componentType = kAudioUnitType_Effect;
        cd.componentSubType = kAudioUnitSubType_HighPassFilter;
        cd.componentManufacturer = kAudioUnitManufacturer_Apple;
        cd.componentFlags = 0;
        cd.componentFlagsMask = 0;
        AUGraphAddNode(audioGraph, &cd, &effectNode);
        AUGraphNodeInfo(audioGraph, effectNode, NULL, &effectUnit);
        AudioUnitSetParameter(effectUnit, kHipassParam_CutoffFrequency, kAudioUnitScope_Global, 0, cutoff, 0);
    } else if (effect == 2) {
        // Slow down or speed up playback.
        float rate = quadraticRandomDecimalDown(1 - PLAYBACK_RATE_MAX_VARIATION * glitchLevel, 1 + PLAYBACK_RATE_MAX_VARIATION * glitchLevel);
        NSLog(@"bus %d: playback rate of %f", bus, rate);

        AudioUnit effectUnit;
        AudioComponentDescription cd;
        cd.componentType = kAudioUnitType_FormatConverter;
        cd.componentSubType = kAudioUnitSubType_Varispeed;
        cd.componentManufacturer = kAudioUnitManufacturer_Apple;
        cd.componentFlags = 0;
        cd.componentFlagsMask = 0;
        AUGraphAddNode(audioGraph, &cd, &effectNode);
        AUGraphNodeInfo(audioGraph, effectNode, NULL, &effectUnit);
        AudioUnitSetParameter(effectUnit, kVarispeedParam_PlaybackRate, kAudioUnitScope_Global, 0, rate, 0);
    } else if (effect == 3) {
        // Desync.
        float minDuration = SAMPLE_DELAY_MIN_OFFSET * sampleRate;
        float durationRange = (SAMPLE_DELAY_MAX_OFFSET - SAMPLE_DELAY_MIN_OFFSET) * glitchLevel * sampleRate;
        int delay = (int)quadraticRandomDecimalTailOut(minDuration, minDuration + durationRange);
        NSLog(@"bus %d: playback delay of %d samples", bus, delay);

        AudioUnit effectUnit;
        AudioComponentDescription cd;
        cd.componentType = kAudioUnitType_Effect;
        cd.componentSubType = kAudioUnitSubType_SampleDelay;
        cd.componentManufacturer = kAudioUnitManufacturer_Apple;
        cd.componentFlags = 0;
        cd.componentFlagsMask = 0;
        AUGraphAddNode(audioGraph, &cd, &effectNode);
        AUGraphNodeInfo(audioGraph, effectNode, NULL, &effectUnit);
        AudioUnitSetParameter(effectUnit, 0 /* in-to-out delay parameter ID */, kAudioUnitScope_Global, 0, delay, 0);
    } else if (effect == 4) {
        // Volume change.
        float gain = quadraticRandomDecimalTailOut(DYNAMICS_PROCESSOR_MIN_VARIATION, DYNAMICS_PROCESSOR_MIN_VARIATION + (DYNAMICS_PROCESSOR_MAX_VARIATION - DYNAMICS_PROCESSOR_MIN_VARIATION) * glitchLevel);
        if (linearRandomInteger(0, 1) == 0) {
            gain = -gain;
        }
        NSLog(@"bus %d: gain of %f dB", bus, gain);

        AudioUnit effectUnit;
        AudioComponentDescription cd;
        cd.componentType = kAudioUnitType_Effect;
        cd.componentSubType = kAudioUnitSubType_DynamicsProcessor;
        cd.componentManufacturer = kAudioUnitManufacturer_Apple;
        cd.componentFlags = 0;
        cd.componentFlagsMask = 0;
        AUGraphAddNode(audioGraph, &cd, &effectNode);
        AUGraphNodeInfo(audioGraph, effectNode, NULL, &effectUnit);
        AudioUnitSetParameter(effectUnit, kDynamicsProcessorParam_Threshold, kAudioUnitScope_Global, 0, 0.0f, 0);
        AudioUnitSetParameter(effectUnit, kDynamicsProcessorParam_HeadRoom, kAudioUnitScope_Global, 0, 0.1f, 0);
        AudioUnitSetParameter(effectUnit, kDynamicsProcessorParam_ExpansionThreshold, kAudioUnitScope_Global, 0, 0.0f, 0);
        AudioUnitSetParameter(effectUnit, kDynamicsProcessorParam_MasterGain, kAudioUnitScope_Global, 0, gain, 0);
    } else if (effect == 5) {
        // Pan.
        float panDirection = quadraticRandomDecimalUp(0.5f - PANNING_MAX_VARIATION * glitchLevel, 0.5f + PANNING_MAX_VARIATION * glitchLevel);
        NSLog(@"bus %d: panning %f", bus, panDirection);

        AudioUnit effectUnit;
        AudioComponentDescription cd;
        cd.componentType = kAudioUnitType_Mixer;
        cd.componentSubType = kAudioUnitSubType_StereoMixer;
        cd.componentManufacturer = kAudioUnitManufacturer_Apple;
        cd.componentFlags = 0;
        cd.componentFlagsMask = 0;
        AUGraphAddNode(audioGraph, &cd, &effectNode);
        AUGraphNodeInfo(audioGraph, effectNode, NULL, &effectUnit);
        AudioUnitSetParameter(effectUnit, kStereoMixerParam_Pan, kAudioUnitScope_Input, 0, panDirection, 0);
    }

    [self connectEffect];
}

- (void)connectEffect {
    AUGraphConnectNodeInput(audioGraph, filePlayerNode, 0, effectNode, 0);
    AUGraphConnectNodeInput(audioGraph, effectNode, 0, mixerNode, bus);
}

- (void)disconnectEffect {
    AUGraphDisconnectNodeInput(audioGraph, mixerNode, bus);
    AUGraphDisconnectNodeInput(audioGraph, effectNode, 0);
    AUGraphRemoveNode(audioGraph, effectNode);
}

@end
